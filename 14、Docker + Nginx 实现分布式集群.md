@toc

# 14、Docker + Nginx 实现分布式集群、负载均衡


## 架构图

![负载均衡、高可用、缓存、RAC架构]($resource/%E8%B4%9F%E8%BD%BD%E5%9D%87%E8%A1%A1%E3%80%81%E9%AB%98%E5%8F%AF%E7%94%A8%E3%80%81%E7%BC%93%E5%AD%98%E3%80%81RAC%E6%9E%B6%E6%9E%84.jpg)

## 默认 nginx.conf 配置

```nginx


user  nginx;
worker_processes  1; # 配置工作线程，配置为你电脑的线程数量 

error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;


events {
    worker_connections  1024;  # 配置一个线程一秒钟可以处理多少个请求
}


http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    keepalive_timeout  65;

    #gzip  on;

    include /etc/nginx/conf.d/*.conf; # 导入虚拟主机
}

```

## 默认虚拟主机配置

```nginx
server {
    listen       80;
    server_name  localhost;

    #charset koi8-r;
    #access_log  /var/log/nginx/host.access.log  main;

    location / {
        root   /usr/share/nginx/html;
        index  index.html index.htm;
    }

    #error_page  404              /404.html;

    # redirect server error pages to the static page /50x.html
    #
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }

    # proxy the PHP scripts to Apache listening on 127.0.0.1:80
    #
    #location ~ \.php$ {
    #    proxy_pass   http://127.0.0.1;
    #}

    # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
    #
    #location ~ \.php$ {
    #    root           html;
    #    fastcgi_pass   127.0.0.1:9000;
    #    fastcgi_index  index.php;
    #    fastcgi_param  SCRIPT_FILENAME  /scripts$fastcgi_script_name;
    #    include        fastcgi_params;
    #}

    # deny access to .htaccess files, if Apache's document root
    # concurs with nginx's one
    #
    #location ~ /\.ht {
    #    deny  all;
    #}
}

```

## 配置反向代理

```nginx
upstream myweb { #myproject为自定义名字
　　#ip_hash;  #开启则代表用ip地址的形式来分配，可解决sesson问题
　　server 127.0.0.1:8080 weight=1; #weight越大，权重越高，被分配的几率越大
　　server 127.0.0.1:8081 weight=1; #我全部在本机，因此用了本地的ip，只要相应换成对应的ip或者域名即可
}
```

## 配置路由

```nginx
location / {　　
      #如果服务器要获取客户端真实IP，可以用下三句设置主机头和客户端真实地址
      #proxy_set_header Host $host;
      #proxy_set_header X-Real-IP $remote_addr;
      #proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

      root   /usr/share/nginx/html;
      index  index.html index.htm;
    　proxy_pass http://myweb;  #myweb为之前在nginx.conf中upstream后定义的名字
 }
```

## 部署多台

```bash
docker run -d -p 8081:8081 --name first-nginx -v ~/workplace/nginx/html:/usr/share/nginx/html -v ~/workplace/nginx/conf/nginx.conf:/etc/nginx/nginx.conf -v ~/workplace/nginx/logs:/var/log/nginx -v ~/workplace/nginx/conf.d:/etc/nginx/conf.d nginx
```


