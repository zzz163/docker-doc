@toc

# 7、Docker-Compse 常用命令


## 基本参数

- `-f`：指定`yml`文件的路径，默认当前目录下的`docker-compose.yml`
- `-p`：指定该项目名称，也就是运行时的`盒子名称`

## **启动构建程序**

```bash
docker-compose up -d
```

`-d`：`damon`，后台运行，也就是所谓的守护进程

## 查看当前项目构建的信息

```bash
docker-compose ps
```

`-a`：查看像详细信息

## 启动程序并重新构建Dockerfile（如果Dockerfile改变了）

```bash
docker-compose up --build
```

## 查看docker-compose.yml 配置文件

```bash
docker-compose config
```

`--节点`：只查询某部分，如：`docker-compose config --services`

## **停止并删除当前构建的所有容器**

```bash
docker-compose down
```

## 查看当前构建的所有镜像

```bash
docker-compose images
```

## 执行构建程序程序中某个容器

```bash
docker-compose exec apj.mysql
```

## 查看构建程序所有日志

```bash
docker-compose logs
```

## 暂停容器

```bash
docker-compose pause [mysql]
```

## 恢复暂停的

```bash
docker-compose unpause [mysql]
```

## **启动某个容器**

```bash
docker-compose start [nginx]
```

## **重启某个容器**

```bash
docker-compose restart [nginx]
```

## **删除某个容器**

```bash
docker-compose rm [nginx]
```

`-f`：强制删除