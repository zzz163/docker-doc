@toc

# 5、Docker-Compose 介绍

Docker-Compose 是一个用户定义和运行多个容器的 Docker 编排步骤文件。

## Docker-Compose 的好处

通常我们一个项目需要集成很多第三方的服务，比如`mssql`，`nginx`、`redis`等等
我们通常通过`docker run`命令一一启动，而且这些命令**非常繁琐，可读性差，不利于复用**！

所以，就有了Docker-Compose，主要是解决上面的问题，我们可以把这些项目依赖的服务和配置信息写在一个`docker-compose.yml`文件中，我们只需要运行这个文件，就可以自动部署好我们项目需要的一切信息。

同时也解决了**二次配置、不能复用、可读性差等问题**

