@toc

# 8、Docker-Compose 转换docker run


## mssql

- docker run 方式

```bash
docker run --name first-mssql -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=APJ.com!q1w2e3' -e 'MSSQL_PID=Express' -p 1433:1433 -d mcr.microsoft.com/mssql/server
```

- docker-compose 方式

```yml
version: '3.4'

services: 
  first-mssql:
    container_name: first-mssql
    environment: 
      - ACCEPT_EULA=Y
      - SA_PASSWORD=APJ.com!q1w2e3
      - MSSQL_PID=Express
    ports: 
      - 1433:1433
    image: mcr.microsoft.com/mssql/server
```

## mysql

- docker run 方式

```bash
docker run --name first-mysql -e MYSQL_ROOT_PASSWORD=123456 -p 3306:3306 -d mysql
```

- docker-compose 方式

```yml
  first-mysql:
    container_name: first-mysql
    environment: 
      - MYSQL_ROOT_PASSWORD=123456
    ports: 
      - 3306:3306
    image: mysql
```

## nginx

- docker run 方式

```bash
docker run --name first-nginx -p 8081:80 -d nginx
```

- docker-compose 方式

```yml
  first-nginx:
    container_name: first-nginx
    ports: 
      - 8081:80
    image: nginx
```

## **完成配置**

```yml
version: '3.4'

services: 
  first-mssql:
    container_name: first-mssql
    environment: 
      - ACCEPT_EULA=Y
      - SA_PASSWORD=APJ.com!q1w2e3
      - MSSQL_PID=Express
    ports: 
      - 1433:1433
    image: mcr.microsoft.com/mssql/server

  first-mysql:
    container_name: first-mysql
    environment: 
      - MYSQL_ROOT_PASSWORD=123456
    ports: 
      - 3306:3306
    image: mysql

  first-nginx:
    container_name: first-nginx
    ports: 
      - 8081:80
    image: nginx
```
