@toc

# 3、Docker run常用命令


## 查看 Docker 版本

```bash
docker version
```

## **下载一个服务镜像**

```bash
docker pull 镜像名称:版本号
```

如果不指定`:版本号`，默认表示下载最新版本

## **运行一个服务**

```bash
docker run 镜像名称:版本号
```

==这个需要详细的章节说明==

## 查看下载的服务镜像

```bash
docker images
```

## **查看当前运行的软件**

```bash
docker container ps
```

查看所有软件（包括非运行的）

```bash
docker container ps -a
```

## 删除服务镜像

```bash
docker rmi 镜像名称1 镜像名称2 ...
```

## 删除正在运行的容器

```bash
docker rm container_id/name1 container_id/name2 ...
```

如果需要强制删除（先停止后删除）,添加`-f`参数

```bash
docker rm -f container_id/name1 container_id/name2 ...
```

## **启动挂起的容器**

```bash
docker start container_id/name1  container_id/name2 ...
```

## **重启容器**

```bash
docker restart container_id/name1  container_id/name2 ...
```

## **查看容器的详细信息**

```bash
docker inspect container_id/name1
```

## **进入容器的命令终端控制台**

```bash
docker exec -it container_id/name1 bash
```

## **查看容器运行日志**

```bash
docker logs container_id/name1 bash
```

## 停止正在运行的容器

```bash
docker stop container_id/name1
```

## **删除所有已经停止的容器**

```bash
docker container prune
```

## 拷贝容器文件

```bash
docker cp container_id/name:目录 当前目录
```
