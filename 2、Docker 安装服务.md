@toc

# 2、Docker 安装服务


## 安装 `Microsoft SQL Server`

### 仓库地址

Docker Hub 文档：[https://hub.docker.com/_/microsoft-mssql-server](https://hub.docker.com/_/microsoft-mssql-server)

微软官方文档：[https://docs.microsoft.com/zh-cn/sql/linux/quickstart-install-connect-docker?view=sql-server-2017&pivots=cs1-bash](https://docs.microsoft.com/zh-cn/sql/linux/quickstart-install-connect-docker?view=sql-server-2017&pivots=cs1-bash)

### 安装脚本

```bash
docker pull mcr.microsoft.com/mssql/server
```

### 运行脚本

```bash
[root@localhost ~]# docker run --name first-mssql -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=APJ.com!q1w2e3' -e 'MSSQL_PID=Express' -p 1433:1433 -d mcr.microsoft.com/mssql/server
38c3504d5c715e71e0912f85f084657f5a6696e51ed8c1309d87137ea77ab9db
[root@localhost ~]# docker container ps
CONTAINER ID        IMAGE                            COMMAND                  CREATED             STATUS              PORTS                    NAMES
38c3504d5c71        mcr.microsoft.com/mssql/server   "/opt/mssql/bin/sqls…"   4 seconds ago       Up 2 seconds        0.0.0.0:1433->1433/tcp   first-mssql
[root@localhost ~]#  
```

### **进入服务环境**

`docker exec -it container_id/name bash`

```bash
[root@localhost ~]# docker exec -it first-mssql bash
root@38c3504d5c71:
```

### 连接数据库

#### 命令方式

```bash
[root@localhost ~]# docker exec -it first-mssql bash
root@38c3504d5c71:/# /opt/mssql-tools/bin/sqlcmd -S localhost -U SA
Password: 
1> create database test_db
2> go
1> select name from sys.databases
2> go
name                                                                                                                            
--------------------------------------------------------------------------------------------------------------------------------
master                                                                                                                          
tempdb                                                                                                                          
model                                                                                                                           
msdb                                                                                                                            
test_db                                                                                                                         

(5 rows affected)
1> use test_db
2> go
Changed database context to 'test_db'.
1> create table staff(id int,name nvarchar(50))
2> go
1> insert into staff(id,name) values(1,'Monk')
2> go

(1 rows affected)
1> select * from staff
2> go
id          name                                              
----------- --------------------------------------------------
          1 Monk                                              

(1 rows affected)
1> 

```

==注：mssql执行sql语句需要调用 `go` 关键字才能执行== 😳

#### 连接工具

![Snipaste_2019-05-09_15-42-21]($resource/Snipaste_2019-05-09_15-42-21.png)

![Snipaste_2019-05-09_15-42-22]($resource/Snipaste_2019-05-09_15-42-22.png)

### **查看服务信息**

`docker inspect container_id/name`

通过该命令，我们可以查看当前运行服务所有信息，类似我们的电脑的系统参数。

```bash
[root@localhost ~]# docker inspect first-mssql
[
    {
        "Id": "38c3504d5c715e71e0912f85f084657f5a6696e51ed8c1309d87137ea77ab9db",
        "Created": "2019-05-09T15:21:55.471266303Z",
        "Path": "/opt/mssql/bin/sqlservr",  // 安装后的路径
        "Args": [],
        "State": {  // 当前状态
            "Status": "running",
            "Running": true,
            "Paused": false,
            "Restarting": false,
            "OOMKilled": false,
            "Dead": false,
            "Pid": 20022,
            "ExitCode": 0,
            "Error": "",
            "StartedAt": "2019-05-09T15:21:56.297146933Z",
            "FinishedAt": "0001-01-01T00:00:00Z"
        },
        "Image": "sha256:885d072870410822379611432e4b4288da79d8e819ec92446b2b55aa532a2871",
        "ResolvConfPath": "/var/lib/docker/containers/38c3504d5c715e71e0912f85f084657f5a6696e51ed8c1309d87137ea77ab9db/resolv.conf",
        "HostnamePath": "/var/lib/docker/containers/38c3504d5c715e71e0912f85f084657f5a6696e51ed8c1309d87137ea77ab9db/hostname",
        "HostsPath": "/var/lib/docker/containers/38c3504d5c715e71e0912f85f084657f5a6696e51ed8c1309d87137ea77ab9db/hosts",
        "LogPath": "/var/lib/docker/containers/38c3504d5c715e71e0912f85f084657f5a6696e51ed8c1309d87137ea77ab9db/38c3504d5c715e71e0912f85f084657f5a6696e51ed8c1309d87137ea77ab9db-json.log",
        "Name": "/first-mssql",  // 容器名称
        "RestartCount": 0,  // 重启次数
        "Driver": "overlay2",  // 网络桥接方式
        "Platform": "linux",  // 当前平台
        "MountLabel": "",
        "ProcessLabel": "",
        "AppArmorProfile": "",
        "ExecIDs": null,
        "HostConfig": {
            "Binds": null,
            "ContainerIDFile": "",
            "LogConfig": {
                "Type": "json-file",
                "Config": {}
            },
            "NetworkMode": "default",
            "PortBindings": {  // 绑定窗口
                "1433/tcp": [
                    {
                        "HostIp": "",
                        "HostPort": "1433"
                    }
                ]
            },
            "RestartPolicy": {
                "Name": "no",
                "MaximumRetryCount": 0
            },
            "AutoRemove": false,
            "VolumeDriver": "",
            "VolumesFrom": null,
            "CapAdd": null,
            "CapDrop": null,
            "Dns": [],  // DNS配置
            "DnsOptions": [],
            "DnsSearch": [],
            "ExtraHosts": null,
            "GroupAdd": null,
            "IpcMode": "shareable",
            "Cgroup": "",
            "Links": null,  // 连接哪个容器
            "OomScoreAdj": 0,
            "PidMode": "",
            "Privileged": false,
            "PublishAllPorts": false,
            "ReadonlyRootfs": false,
            "SecurityOpt": null,
            "UTSMode": "",
            "UsernsMode": "",
            "ShmSize": 67108864,
            "Runtime": "runc",
            "ConsoleSize": [
                0,
                0
            ],
            "Isolation": "",
            "CpuShares": 0,
            "Memory": 0,
            "NanoCpus": 0,
            "CgroupParent": "",
            "BlkioWeight": 0,
            "BlkioWeightDevice": [],
            "BlkioDeviceReadBps": null,
            "BlkioDeviceWriteBps": null,
            "BlkioDeviceReadIOps": null,
            "BlkioDeviceWriteIOps": null,
            "CpuPeriod": 0,
            "CpuQuota": 0,
            "CpuRealtimePeriod": 0,
            "CpuRealtimeRuntime": 0,
            "CpusetCpus": "",
            "CpusetMems": "",
            "Devices": [],
            "DeviceCgroupRules": null,
            "DiskQuota": 0,
            "KernelMemory": 0,
            "MemoryReservation": 0,
            "MemorySwap": 0,
            "MemorySwappiness": null,
            "OomKillDisable": false,
            "PidsLimit": 0,
            "Ulimits": null,
            "CpuCount": 0,
            "CpuPercent": 0,
            "IOMaximumIOps": 0,
            "IOMaximumBandwidth": 0,
            "MaskedPaths": [
                "/proc/asound",
                "/proc/acpi",
                "/proc/kcore",
                "/proc/keys",
                "/proc/latency_stats",
                "/proc/timer_list",
                "/proc/timer_stats",
                "/proc/sched_debug",
                "/proc/scsi",
                "/sys/firmware"
            ],
            "ReadonlyPaths": [
                "/proc/bus",
                "/proc/fs",
                "/proc/irq",
                "/proc/sys",
                "/proc/sysrq-trigger"
            ]
        },
        "GraphDriver": {
            "Data": {
                "LowerDir": "/var/lib/docker/overlay2/55d4f81a6507095bb9589443382c8040660b291a213cbc50d655d47dbe9c5428-init/diff:/var/lib/docker/overlay2/92f9f44f59e64f2651d8200d7869e573acb3bbfe89dd5b6546082ac071af9c88/diff:/var/lib/docker/overlay2/59ca9dc3b3813175e2cca35218b0264cb920622954bcece69c0cede99ab786e4/diff:/var/lib/docker/overlay2/8f1c1741b4a0886a3345fb9872d7c9bb5d280c19bda0fcb332f95cc64b4371d3/diff:/var/lib/docker/overlay2/3fed9c5090c3b92f6c343a74010e9aad66a1157310903735b763317ef91cc22a/diff:/var/lib/docker/overlay2/65adeef26028cba5806f6f57279d7abd9aff1e5b07fe8fbc5ab082b84505eb20/diff:/var/lib/docker/overlay2/f91c46215f0b4e1a0e0500639d0339f40e7be13b862a94a706c38d6f12c9155e/diff:/var/lib/docker/overlay2/173eff48d869a7024e15cee7854dde29a66c1bb96e4e6ff033e8b633718f10b1/diff:/var/lib/docker/overlay2/e1bf22bd52567e1e60ef1c7f09777dfb14cc66810498b340ac5b326a8c170964/diff:/var/lib/docker/overlay2/0a31bf08dacf3d9334cf9d671ef3202ddd9eaa80ac59f295d64546f74e35d0e5/diff:/var/lib/docker/overlay2/7eaa882f647a1110df701ddc8b00747621c20d91365ced5422d1ba4887576b8a/diff",
                "MergedDir": "/var/lib/docker/overlay2/55d4f81a6507095bb9589443382c8040660b291a213cbc50d655d47dbe9c5428/merged",
                "UpperDir": "/var/lib/docker/overlay2/55d4f81a6507095bb9589443382c8040660b291a213cbc50d655d47dbe9c5428/diff",
                "WorkDir": "/var/lib/docker/overlay2/55d4f81a6507095bb9589443382c8040660b291a213cbc50d655d47dbe9c5428/work"
            },
            "Name": "overlay2"
        },
        "Mounts": [],  //挂载卷
        "Config": {
            "Hostname": "38c3504d5c71",
            "Domainname": "",
            "User": "",
            "AttachStdin": false,
            "AttachStdout": false,
            "AttachStderr": false,
            "ExposedPorts": {  // 导出端口
                "1433/tcp": {}
            },
            "Tty": false,
            "OpenStdin": false,
            "StdinOnce": false,
            "Env": [  // 环境变量！！！！！！
                "ACCEPT_EULA=Y",
                "SA_PASSWORD=APJ.com!q1w2e3",
                "MSSQL_PID=Express",
                "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
            ],
            "Cmd": [  // 命令方式
                "/opt/mssql/bin/sqlservr"
            ],
            "ArgsEscaped": true,
            "Image": "mcr.microsoft.com/mssql/server",  // 镜像
            "Volumes": null,  // 数据卷
            "WorkingDir": "",
            "Entrypoint": null,
            "OnBuild": null,
            "Labels": {  // 版本信息
                "com.microsoft.product": "Microsoft SQL Server",
                "com.microsoft.version": "14.0.3038.14",
                "vendor": "Microsoft"
            }
        },
        "NetworkSettings": {
            "Bridge": "",
            "SandboxID": "e46db15024f097084f16870f69d6613e7598206de093b4392b45a25792c67567",
            "HairpinMode": false,
            "LinkLocalIPv6Address": "",
            "LinkLocalIPv6PrefixLen": 0,
            "Ports": {  // 映射端口
                "1433/tcp": [
                    {
                        "HostIp": "0.0.0.0",
                        "HostPort": "1433"
                    }
                ]
            },
            "SandboxKey": "/var/run/docker/netns/e46db15024f0",
            "SecondaryIPAddresses": null,
            "SecondaryIPv6Addresses": null,
            "EndpointID": "cd1b42bf173c3ca2b6f80a4848c98044f8d984c349af6938f90498e51ebc0878",
            "Gateway": "172.17.0.1",
            "GlobalIPv6Address": "",
            "GlobalIPv6PrefixLen": 0,
            "IPAddress": "172.17.0.2",  // 当前容器IP地址
            "IPPrefixLen": 16,
            "IPv6Gateway": "",
            "MacAddress": "02:42:ac:11:00:02",
            "Networks": {  // 网络信息
                "bridge": {
                    "IPAMConfig": null,
                    "Links": null,
                    "Aliases": null,
                    "NetworkID": "16770dafaf089bb392c72e96f2f2d1ea5e867b80878d0055e509b604ad706583",
                    "EndpointID": "cd1b42bf173c3ca2b6f80a4848c98044f8d984c349af6938f90498e51ebc0878",
                    "Gateway": "172.17.0.1",
                    "IPAddress": "172.17.0.2",
                    "IPPrefixLen": 16,
                    "IPv6Gateway": "",
                    "GlobalIPv6Address": "",
                    "GlobalIPv6PrefixLen": 0,
                    "MacAddress": "02:42:ac:11:00:02",
                    "DriverOpts": null
                }
            }
        }
    }
]
[root@localhost ~]# 

```


### **查看服务日志**

`docker logs container_id/name`

通常我们使用该命令查看服务运行错误日志

### 常见错误

- 内存不足

`mssql` 要求内存必须大于 ==3.5G== 😳

- 密码强度

`mssql` 要求密码必须至少为 8 个字符长，且包含三个以下四种字符集的字符：大写字母、 小写字母、 十进制数字和符号 😳

通过 `docker logs container_id/name` 可以查看具体错误日志

### 实现多开

```bash
[root@localhost ~]# docker run --name two-mssql -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=APJ.com!q1w2e3' -e 'MSSQL_PID=Express' -p 8090:1433 -d mcr.microsoft.com/mssql/server
6860fb5d10237e4bd1931f46c80b4fb4ca33e77db61b50753e1fa999324483d2
[root@localhost ~]# 
```

### **Docker 环境中使用本地（宿主）数据库**

在docker环境中，如果想访问宿主的服务，比如`mssql`，`redis`，`mysql`等等就不能使用`localhost`，==`localhost`指的是docker环境中的地址，并非我们宿主的地址==，所以docker提供了其他模式来访问`本机（宿主）的localhost`

- Mac/Windows

`host.docker.internal`，这个就相当于我们的以前的`localhost`

- Linux

![Snipaste_2019-05-09_15-42-23]($resource/Snipaste_2019-05-09_15-42-23.png)

---

## 安装 `Mysql`

### 安装脚本

```bash
docker pull mysql
```

### 运行服务

```bash
[root@localhost ~]# docker run --name first-mysql -e MYSQL_ROOT_PASSWORD=123456 -p 3306:3306 -d mysql
456619e7a1a7616a8b0e7cd720df00a0dc5afbbbafe0065dd8cd0f7a5b250e7a
[root@localhost ~]# docker container ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                               NAMES
456619e7a1a7        mysql               "docker-entrypoint.s…"   7 seconds ago       Up 6 seconds        0.0.0.0:3306->3306/tcp, 33060/tcp   first-mysql
[root@localhost ~]# 
```

### 连接数据库

#### 命令方式

```bash
[root@localhost ~]# docker exec -it first-mysql bash
root@456619e7a1a7:/# mysql -uroot -p
Enter password: 
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 8
Server version: 8.0.16 MySQL Community Server - GPL

Copyright (c) 2000, 2019, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> create database test_db;
Query OK, 1 row affected (0.00 sec)

mysql> use test_db;
Database changed
mysql> create table staff(id int,name nvarchar(50))
    -> ;
Query OK, 0 rows affected, 1 warning (0.01 sec)

mysql> insert into staff(id,name) values(1,'Monk');
Query OK, 1 row affected (0.01 sec)

mysql> select * from staff;
+------+------+
| id   | name |
+------+------+
|    1 | Monk |
+------+------+
1 row in set (0.00 sec)

mysql>
```

#### 连接工具

![Snipaste_2019-05-09_15-42-24]($resource/Snipaste_2019-05-09_15-42-24.png)

![Snipaste_2019-05-09_15-42-25]($resource/Snipaste_2019-05-09_15-42-25.png)

### 查看服务信息

```bash
[root@localhost ~]# docker inspect first-mysql
[
    {
        "Id": "456619e7a1a7616a8b0e7cd720df00a0dc5afbbbafe0065dd8cd0f7a5b250e7a",
        "Created": "2019-05-10T01:11:50.758145696Z",
        "Path": "docker-entrypoint.sh",
        "Args": [
            "mysqld"
        ],
        "State": {
            "Status": "running",
            "Running": true,
            "Paused": false,
            "Restarting": false,
            "OOMKilled": false,
            "Dead": false,
            "Pid": 20269,
            "ExitCode": 0,
            "Error": "",
            "StartedAt": "2019-05-10T01:11:51.433785287Z",
            "FinishedAt": "0001-01-01T00:00:00Z"
        },
        "Image": "sha256:990386cbd5c04effd6669ab958aa41ce06052bbe52b8d88f259bfadf9fbf37c9",
        "ResolvConfPath": "/var/lib/docker/containers/456619e7a1a7616a8b0e7cd720df00a0dc5afbbbafe0065dd8cd0f7a5b250e7a/resolv.conf",
        "HostnamePath": "/var/lib/docker/containers/456619e7a1a7616a8b0e7cd720df00a0dc5afbbbafe0065dd8cd0f7a5b250e7a/hostname",
        "HostsPath": "/var/lib/docker/containers/456619e7a1a7616a8b0e7cd720df00a0dc5afbbbafe0065dd8cd0f7a5b250e7a/hosts",
        "LogPath": "/var/lib/docker/containers/456619e7a1a7616a8b0e7cd720df00a0dc5afbbbafe0065dd8cd0f7a5b250e7a/456619e7a1a7616a8b0e7cd720df00a0dc5afbbbafe0065dd8cd0f7a5b250e7a-json.log",
        "Name": "/first-mysql",
        "RestartCount": 0,
        "Driver": "overlay2",
        "Platform": "linux",
        "MountLabel": "",
        "ProcessLabel": "",
        "AppArmorProfile": "",
        "ExecIDs": null,
        "HostConfig": {
            "Binds": null,
            "ContainerIDFile": "",
            "LogConfig": {
                "Type": "json-file",
                "Config": {}
            },
            "NetworkMode": "default",
            "PortBindings": {
                "3306/tcp": [
                    {
                        "HostIp": "",
                        "HostPort": "3306"
                    }
                ]
            },
            "RestartPolicy": {
                "Name": "no",
                "MaximumRetryCount": 0
            },
            "AutoRemove": false,
            "VolumeDriver": "",
            "VolumesFrom": null,
            "CapAdd": null,
            "CapDrop": null,
            "Dns": [],
            "DnsOptions": [],
            "DnsSearch": [],
            "ExtraHosts": null,
            "GroupAdd": null,
            "IpcMode": "shareable",
            "Cgroup": "",
            "Links": null,
            "OomScoreAdj": 0,
            "PidMode": "",
            "Privileged": false,
            "PublishAllPorts": false,
            "ReadonlyRootfs": false,
            "SecurityOpt": null,
            "UTSMode": "",
            "UsernsMode": "",
            "ShmSize": 67108864,
            "Runtime": "runc",
            "ConsoleSize": [
                0,
                0
            ],
            "Isolation": "",
            "CpuShares": 0,
            "Memory": 0,
            "NanoCpus": 0,
            "CgroupParent": "",
            "BlkioWeight": 0,
            "BlkioWeightDevice": [],
            "BlkioDeviceReadBps": null,
            "BlkioDeviceWriteBps": null,
            "BlkioDeviceReadIOps": null,
            "BlkioDeviceWriteIOps": null,
            "CpuPeriod": 0,
            "CpuQuota": 0,
            "CpuRealtimePeriod": 0,
            "CpuRealtimeRuntime": 0,
            "CpusetCpus": "",
            "CpusetMems": "",
            "Devices": [],
            "DeviceCgroupRules": null,
            "DiskQuota": 0,
            "KernelMemory": 0,
            "MemoryReservation": 0,
            "MemorySwap": 0,
            "MemorySwappiness": null,
            "OomKillDisable": false,
            "PidsLimit": 0,
            "Ulimits": null,
            "CpuCount": 0,
            "CpuPercent": 0,
            "IOMaximumIOps": 0,
            "IOMaximumBandwidth": 0,
            "MaskedPaths": [
                "/proc/asound",
                "/proc/acpi",
                "/proc/kcore",
                "/proc/keys",
                "/proc/latency_stats",
                "/proc/timer_list",
                "/proc/timer_stats",
                "/proc/sched_debug",
                "/proc/scsi",
                "/sys/firmware"
            ],
            "ReadonlyPaths": [
                "/proc/bus",
                "/proc/fs",
                "/proc/irq",
                "/proc/sys",
                "/proc/sysrq-trigger"
            ]
        },
        "GraphDriver": {
            "Data": {
                "LowerDir": "/var/lib/docker/overlay2/0a2d04b043a3e214e0563b45511a265a03ae16290cc25710e98d0f53d3d7ea02-init/diff:/var/lib/docker/overlay2/044af1473a6979d656c17f0f728fc6a9c3053e17e816745d22443e96bb4fbc00/diff:/var/lib/docker/overlay2/cfb0bd00236b7ffbca52426a1a0fde68c222fd17aadd6279b295375edf88e355/diff:/var/lib/docker/overlay2/7311be14646ced4c9939eb08c5a39c672228d51627cc2ad998e2b37fc67c65e9/diff:/var/lib/docker/overlay2/8c2e41246e9e09273a5b3ab20d91848745cbfd2784bef6fe0e8922d8f826a832/diff:/var/lib/docker/overlay2/d69477aa0896dc929662a9a8ebdf140c2d7b8069c607c566093bb2b7b4e2b697/diff:/var/lib/docker/overlay2/67819845ee316467e926251766be0c2f0622d6ccd094aef7e08ce9e717dc0082/diff:/var/lib/docker/overlay2/21e84adc6498658f1cffe4fe1765a60bd44f4aed93bcf2d889dbd1a478c929a9/diff:/var/lib/docker/overlay2/92092c529e6637c7fbecbe834e363970f23b0a72574a8864d2040334021bb187/diff:/var/lib/docker/overlay2/39c17b287839fcc8868cf607b615d8900e6fab65e80e3911b8cf370ca06ae643/diff:/var/lib/docker/overlay2/416eb241c5b95524e7bd18b55930ea345e83f35bc727338484844b6e2ea8ce66/diff:/var/lib/docker/overlay2/909255867df95e576f396d775e6f5a14effe86497e96b3b41a081a1c547ded78/diff:/var/lib/docker/overlay2/c4d23277fe59b7a3e17c49e182d25bece0b9e0ab4ff3aa26871fec32ee56ae9e/diff",
                "MergedDir": "/var/lib/docker/overlay2/0a2d04b043a3e214e0563b45511a265a03ae16290cc25710e98d0f53d3d7ea02/merged",
                "UpperDir": "/var/lib/docker/overlay2/0a2d04b043a3e214e0563b45511a265a03ae16290cc25710e98d0f53d3d7ea02/diff",
                "WorkDir": "/var/lib/docker/overlay2/0a2d04b043a3e214e0563b45511a265a03ae16290cc25710e98d0f53d3d7ea02/work"
            },
            "Name": "overlay2"
        },
        "Mounts": [
            {
                "Type": "volume",
                "Name": "3fd3fd86c2d2a399c2099ee25b73fa48fd2a97cf6f3fb4dc75e664ef6d2a2df8",
                "Source": "/var/lib/docker/volumes/3fd3fd86c2d2a399c2099ee25b73fa48fd2a97cf6f3fb4dc75e664ef6d2a2df8/_data",
                "Destination": "/var/lib/mysql",
                "Driver": "local",
                "Mode": "",
                "RW": true,
                "Propagation": ""
            }
        ],
        "Config": {
            "Hostname": "456619e7a1a7",
            "Domainname": "",
            "User": "",
            "AttachStdin": false,
            "AttachStdout": false,
            "AttachStderr": false,
            "ExposedPorts": {
                "3306/tcp": {},
                "33060/tcp": {}
            },
            "Tty": false,
            "OpenStdin": false,
            "StdinOnce": false,
            "Env": [
                "MYSQL_ROOT_PASSWORD=123456",
                "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
                "GOSU_VERSION=1.7",
                "MYSQL_MAJOR=8.0",
                "MYSQL_VERSION=8.0.16-2debian9"
            ],
            "Cmd": [
                "mysqld"
            ],
            "ArgsEscaped": true,
            "Image": "mysql",
            "Volumes": {
                "/var/lib/mysql": {}
            },
            "WorkingDir": "",
            "Entrypoint": [
                "docker-entrypoint.sh"
            ],
            "OnBuild": null,
            "Labels": {}
        },
        "NetworkSettings": {
            "Bridge": "",
            "SandboxID": "be959aea9dd878556d0f4cfd502fb1facc1978c0ef4eb09fa5245a88ac6d8e46",
            "HairpinMode": false,
            "LinkLocalIPv6Address": "",
            "LinkLocalIPv6PrefixLen": 0,
            "Ports": {
                "3306/tcp": [
                    {
                        "HostIp": "0.0.0.0",
                        "HostPort": "3306"
                    }
                ],
                "33060/tcp": null
            },
            "SandboxKey": "/var/run/docker/netns/be959aea9dd8",
            "SecondaryIPAddresses": null,
            "SecondaryIPv6Addresses": null,
            "EndpointID": "109f9b91a94d1316083917aa9500b8a3632be7f20a795701a8a1e9314708cad5",
            "Gateway": "172.17.0.1",
            "GlobalIPv6Address": "",
            "GlobalIPv6PrefixLen": 0,
            "IPAddress": "172.17.0.2",
            "IPPrefixLen": 16,
            "IPv6Gateway": "",
            "MacAddress": "02:42:ac:11:00:02",
            "Networks": {
                "bridge": {
                    "IPAMConfig": null,
                    "Links": null,
                    "Aliases": null,
                    "NetworkID": "08a729b1639d17e916360a2eb51434ee72db0d6d3a68fd9e00bd9ac4d6c6e2ef",
                    "EndpointID": "109f9b91a94d1316083917aa9500b8a3632be7f20a795701a8a1e9314708cad5",
                    "Gateway": "172.17.0.1",
                    "IPAddress": "172.17.0.2",
                    "IPPrefixLen": 16,
                    "IPv6Gateway": "",
                    "GlobalIPv6Address": "",
                    "GlobalIPv6PrefixLen": 0,
                    "MacAddress": "02:42:ac:11:00:02",
                    "DriverOpts": null
                }
            }
        }
    }
]
[root@localhost ~]# 
```

----

## 安装 `Nginx`

### 安装脚本

```bash
docker pull nginx
```

### 运行服务

```bash
[root@localhost ~]# docker run --name first-nginx -p 8081:80 -d nginx
48817f1d0fbe297bc88782ef602055841272e2b01758104d31d82a82e7b58edf
[root@localhost ~]# docker container ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                               NAMES
48817f1d0fbe        nginx               "nginx -g 'daemon of…"   5 seconds ago       Up 3 seconds        0.0.0.0:8081->80/tcp                first-nginx
456619e7a1a7        mysql               "docker-entrypoint.s…"   14 minutes ago      Up 14 minutes       0.0.0.0:3306->3306/tcp, 33060/tcp   first-mysql
[root@localhost ~]#
```

### 查看Nginx

![Snipaste_2019-05-09_15-42-26]($resource/Snipaste_2019-05-09_15-42-26.png)

### 查看Nginx 配置信息

```bash
[root@localhost ~]# docker inspect first-nginx
[
    {
        "Id": "48817f1d0fbe297bc88782ef602055841272e2b01758104d31d82a82e7b58edf",
        "Created": "2019-05-10T01:25:57.571934272Z",
        "Path": "nginx",
        "Args": [
            "-g",
            "daemon off;"
        ],
        "State": {
            "Status": "running",
            "Running": true,
            "Paused": false,
            "Restarting": false,
            "OOMKilled": false,
            "Dead": false,
            "Pid": 20663,
            "ExitCode": 0,
            "Error": "",
            "StartedAt": "2019-05-10T01:25:59.165203738Z",
            "FinishedAt": "0001-01-01T00:00:00Z"
        },
        "Image": "sha256:53f3fd8007f76bd23bf663ad5f5009c8941f63828ae458cef584b5f85dc0a7bf",
        "ResolvConfPath": "/var/lib/docker/containers/48817f1d0fbe297bc88782ef602055841272e2b01758104d31d82a82e7b58edf/resolv.conf",
        "HostnamePath": "/var/lib/docker/containers/48817f1d0fbe297bc88782ef602055841272e2b01758104d31d82a82e7b58edf/hostname",
        "HostsPath": "/var/lib/docker/containers/48817f1d0fbe297bc88782ef602055841272e2b01758104d31d82a82e7b58edf/hosts",
        "LogPath": "/var/lib/docker/containers/48817f1d0fbe297bc88782ef602055841272e2b01758104d31d82a82e7b58edf/48817f1d0fbe297bc88782ef602055841272e2b01758104d31d82a82e7b58edf-json.log",
        "Name": "/first-nginx",
        "RestartCount": 0,
        "Driver": "overlay2",
        "Platform": "linux",
        "MountLabel": "",
        "ProcessLabel": "",
        "AppArmorProfile": "",
        "ExecIDs": null,
        "HostConfig": {
            "Binds": null,
            "ContainerIDFile": "",
            "LogConfig": {
                "Type": "json-file",
                "Config": {}
            },
            "NetworkMode": "default",
            "PortBindings": {
                "80/tcp": [
                    {
                        "HostIp": "",
                        "HostPort": "8081"
                    }
                ]
            },
            "RestartPolicy": {
                "Name": "no",
                "MaximumRetryCount": 0
            },
            "AutoRemove": false,
            "VolumeDriver": "",
            "VolumesFrom": null,
            "CapAdd": null,
            "CapDrop": null,
            "Dns": [],
            "DnsOptions": [],
            "DnsSearch": [],
            "ExtraHosts": null,
            "GroupAdd": null,
            "IpcMode": "shareable",
            "Cgroup": "",
            "Links": null,
            "OomScoreAdj": 0,
            "PidMode": "",
            "Privileged": false,
            "PublishAllPorts": false,
            "ReadonlyRootfs": false,
            "SecurityOpt": null,
            "UTSMode": "",
            "UsernsMode": "",
            "ShmSize": 67108864,
            "Runtime": "runc",
            "ConsoleSize": [
                0,
                0
            ],
            "Isolation": "",
            "CpuShares": 0,
            "Memory": 0,
            "NanoCpus": 0,
            "CgroupParent": "",
            "BlkioWeight": 0,
            "BlkioWeightDevice": [],
            "BlkioDeviceReadBps": null,
            "BlkioDeviceWriteBps": null,
            "BlkioDeviceReadIOps": null,
            "BlkioDeviceWriteIOps": null,
            "CpuPeriod": 0,
            "CpuQuota": 0,
            "CpuRealtimePeriod": 0,
            "CpuRealtimeRuntime": 0,
            "CpusetCpus": "",
            "CpusetMems": "",
            "Devices": [],
            "DeviceCgroupRules": null,
            "DiskQuota": 0,
            "KernelMemory": 0,
            "MemoryReservation": 0,
            "MemorySwap": 0,
            "MemorySwappiness": null,
            "OomKillDisable": false,
            "PidsLimit": 0,
            "Ulimits": null,
            "CpuCount": 0,
            "CpuPercent": 0,
            "IOMaximumIOps": 0,
            "IOMaximumBandwidth": 0,
            "MaskedPaths": [
                "/proc/asound",
                "/proc/acpi",
                "/proc/kcore",
                "/proc/keys",
                "/proc/latency_stats",
                "/proc/timer_list",
                "/proc/timer_stats",
                "/proc/sched_debug",
                "/proc/scsi",
                "/sys/firmware"
            ],
            "ReadonlyPaths": [
                "/proc/bus",
                "/proc/fs",
                "/proc/irq",
                "/proc/sys",
                "/proc/sysrq-trigger"
            ]
        },
        "GraphDriver": {
            "Data": {
                "LowerDir": "/var/lib/docker/overlay2/61a69e59fd1c2dc3030339fc0d2a6a22cc516e3699c40e51dbce787aa4544be7-init/diff:/var/lib/docker/overlay2/4f5132da5fc7f00363f730d0937e14131b5ee8145e3a39ee0bdcb2c1ce06045b/diff:/var/lib/docker/overlay2/ae2f75ea5c367d2276c5afd2b37da9c998b86f2db96cb96ed59faf87e088091b/diff:/var/lib/docker/overlay2/c4d23277fe59b7a3e17c49e182d25bece0b9e0ab4ff3aa26871fec32ee56ae9e/diff",
                "MergedDir": "/var/lib/docker/overlay2/61a69e59fd1c2dc3030339fc0d2a6a22cc516e3699c40e51dbce787aa4544be7/merged",
                "UpperDir": "/var/lib/docker/overlay2/61a69e59fd1c2dc3030339fc0d2a6a22cc516e3699c40e51dbce787aa4544be7/diff",
                "WorkDir": "/var/lib/docker/overlay2/61a69e59fd1c2dc3030339fc0d2a6a22cc516e3699c40e51dbce787aa4544be7/work"
            },
            "Name": "overlay2"
        },
        "Mounts": [],
        "Config": {
            "Hostname": "48817f1d0fbe",
            "Domainname": "",
            "User": "",
            "AttachStdin": false,
            "AttachStdout": false,
            "AttachStderr": false,
            "ExposedPorts": {
                "80/tcp": {}
            },
            "Tty": false,
            "OpenStdin": false,
            "StdinOnce": false,
            "Env": [
                "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
                "NGINX_VERSION=1.15.12-1~stretch",
                "NJS_VERSION=1.15.12.0.3.1-1~stretch"
            ],
            "Cmd": [
                "nginx",
                "-g",
                "daemon off;"
            ],
            "ArgsEscaped": true,
            "Image": "nginx",
            "Volumes": null,
            "WorkingDir": "",
            "Entrypoint": null,
            "OnBuild": null,
            "Labels": {
                "maintainer": "NGINX Docker Maintainers <docker-maint@nginx.com>"
            },
            "StopSignal": "SIGTERM"
        },
        "NetworkSettings": {
            "Bridge": "",
            "SandboxID": "b50ac6df4fd5f84e682977723298665773dcff97a22634b7ce25f48de4409343",
            "HairpinMode": false,
            "LinkLocalIPv6Address": "",
            "LinkLocalIPv6PrefixLen": 0,
            "Ports": {
                "80/tcp": [
                    {
                        "HostIp": "0.0.0.0",
                        "HostPort": "8081"
                    }
                ]
            },
            "SandboxKey": "/var/run/docker/netns/b50ac6df4fd5",
            "SecondaryIPAddresses": null,
            "SecondaryIPv6Addresses": null,
            "EndpointID": "aa0030270df61d32ded4efab01ade64207739cf300f6c39da83f0cb059490fb3",
            "Gateway": "172.17.0.1",
            "GlobalIPv6Address": "",
            "GlobalIPv6PrefixLen": 0,
            "IPAddress": "172.17.0.3",
            "IPPrefixLen": 16,
            "IPv6Gateway": "",
            "MacAddress": "02:42:ac:11:00:03",
            "Networks": {
                "bridge": {
                    "IPAMConfig": null,
                    "Links": null,
                    "Aliases": null,
                    "NetworkID": "08a729b1639d17e916360a2eb51434ee72db0d6d3a68fd9e00bd9ac4d6c6e2ef",
                    "EndpointID": "aa0030270df61d32ded4efab01ade64207739cf300f6c39da83f0cb059490fb3",
                    "Gateway": "172.17.0.1",
                    "IPAddress": "172.17.0.3",
                    "IPPrefixLen": 16,
                    "IPv6Gateway": "",
                    "GlobalIPv6Address": "",
                    "GlobalIPv6PrefixLen": 0,
                    "MacAddress": "02:42:ac:11:00:03",
                    "DriverOpts": null
                }
            }
        }
    }
]
[root@localhost ~]# 
```

### **查看Nginx 配置文件及目录信息**

```bash
[root@localhost ~]# docker exec -it first-nginx bash
root@48817f1d0fbe:/# ls   
bin  boot  dev	etc  home  lib	lib64  media  mnt  opt	proc  root  run  sbin  srv  sys  tmp  usr  var
root@48817f1d0fbe:/# cd etc/nginx
root@48817f1d0fbe:/etc/nginx# ls
conf.d	fastcgi_params	koi-utf  koi-win  mime.types  modules  nginx.conf  scgi_params	uwsgi_params  win-utf
root@48817f1d0fbe:/etc/nginx# 

```

- 配置文件存放路径：`/etc/nginx/nginx.conf`
- 虚拟主机存放路径：`/etc/nginx/conf.d`
- 日志存放路径：`/var/log/nginx`
- 静态资源存放路径：`/usr/share/nginx/html`

```bash
docker run -d -p 8082:8082 --name two-nginx -v ~/workplace/nginx/html:/usr/share/nginx/html -v ~/workplace/nginx/conf/nginx.conf:/etc/nginx/nginx.conf -v ~/workplace/nginx/logs:/var/log/nginx -v ~/workplace/nginx/conf.d:/etc/nginx/conf.d nginx
```

### **编辑服务中某个文件**

访问容器某个文件

`docker cp container_id/name:路径`

```bash
[root@localhost ~]# docker cp first-nginx:/usr/share/nginx/html/index.html .
[root@localhost ~]# ls
```
