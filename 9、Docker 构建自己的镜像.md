@toc

# 9、Docker 构建自己的镜像


## 先看一个例子

[https://github.com/SeriaWei/ZKEACMS](https://github.com/SeriaWei/ZKEACMS)

```bash
docker run -d -p 5001:80 zkeasoft/zkeacms
```

![Snipaste_2019-05-09_15-42-21]($resource/Snipaste_2019-05-09_15-42-21.png)

