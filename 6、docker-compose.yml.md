@toc

# 6、docker-compose.yml


上一节中我们已经了解，我们项目所有的依赖都写在了`docker-compose.yml`中，那我们如何编写这个文件呢？

## 准备工具

- Visual Studio Code：安装`docker`拓展插件即可（==推荐==）
- 黑客/高手/装逼佬：`vim/vi`或者直接记事本（~~新手勿入~~）

## 常用配置

```yaml
version: '3.4'

services:
  apj.image_name: # 服务名称，用于重启，启动，暂停等操作
    image: monksoul/image_name # 可以动态构建，指定构建后的名称
    container_name: container_name # 运行时的容器名称
    build:  # 构建参数
      context: .  # 指定当前路径
      dockerfile: Dockerfile  # Dockerfile文件名
    volumes: # 数据卷
      - ${APPDATA}/Microsoft/UserSecrets:/root/.microsoft/usersecrets:ro
      - ${APPDATA}/ASP.NET/Https:/root/.aspnet/https:ro
    depends_on:  #依赖项
      - apj.mysql
    expose:  #导出80端口
      - 80
    ports:  # 绑定端口
      - 8082:80
    links: # 配置域网络  (已被弃用，推荐 networkds）
      - apj.mysql:link_sql
    external_links: 连接外部网络（非当前docker-compose构建的）
      - other.mysql:link_sql2
    environment:  # 配置环境变量
      - NAME=monk
    networks: # 指定网络类型
      - webnet

  apj.mysql:
    image: mysql
    restart: always
    container_name: monk.mysql
    environment: 
      - MYSQL_ROOT_PASSWORD=123456 
    ports: 
      - 3306:3306
    networks: 
      - webnet

    deploy: # 部署方式
      replicas: 5 # 克隆5个，一键部署多个容器当前
      resources: # 配置资源
        limits: # 资源配置
          cpus: "0.1" # 本机cpu的10%
          memory: 50M # 当前容器最大内存
      restart_policy: # 重启策略
        condition: on-failure # 条件：失败就重启

networks: # 创建新的网络类型
  webnet:
    driver: bridge  # 网络方式：桥接或集群
```

## 特别提醒

默认如果没有指定具体的compose配置文件，那么就docker-compose.yml，并且如果不指定项目名称，默认就是文件夹的name

指定其他命名方式

```bash
docker-compose -f new-name.yml up

docker-compose -f new-name.yml ps
```

`-f`表示`flle`

----

## 其他

每一个docker-compose.yml文件的第一个便是`version`节点，指定了我们当前docker-compose.yml的语法版本，目前最新`3.4`