@toc

# 4、Docker run 常用命令

[https://docs.docker.com/engine/reference/commandline/run/](https://docs.docker.com/engine/reference/commandline/run/)

## 基本语法

```bash
docker run 参数集合 镜像名称:版本号
```

==参数集合语法：==

`-参数名缩写符 参数值`，如：`-p 80:80`

`--参数名全名称 参数值`，如：`--name my-nginx`

## 参数名解析

- `-d`：[单词：damon，守护进程]：后台运行，==不指定的话，默认只运行一次就会自动关闭==

- `-p`：[单词：ports，端口] ：配置软件对外端口，==主机端口:服务端口==

- `--name`：[单词：name]：指定软件运行时名称，==主要用来替换 container_id==

- `-e`：[单词：environment，环境]：配置软件运行起来需要配置的环境变量，==以后我们所有可配置的东西都应该弄成环境变量方式==

- `--restart`:[单词：restart]：配置软件启动方式，默认值：`no`，==随“机”自启：always==

- `-it`：比较特别的指令，运行服务并分配一个伪终端控制台到命令行，==进入容器的命令终端控制台==

- `--rm`：[单词：remove]，运行一个镜像，如果存在先删除

- `--link`：连接某个容器，==让当前容器和自己处于同一个域网络中==（~~已经被启用~~）用`--network`代替

- `-v或--volume`：指定一个数据卷，支持多个，==本机地址：容器里面地址==

 在`Windows`系统中，本机地址需要全路径，==盘符前面需要加两个斜杠==，如：`//D/workplace` 表示D盘workplace目录

在`Linux/Mac`系统中，本机地址需要添加==~==开头，如：`~/etc` 表示根目录的etc目录，linux系统很少有盘符的概念（当然也可以有）

- `--volumes-from`：共用其他容器的数据卷，支持多个，==--volumes-from 容器名称或id==

- `--network`：指定一个域网络，默认值 `host`，表示和主机同一个网络，==配置自定义网络：--network=自定义网络==

- `--ip`：指定容器运行时的ip地址，可通过`docker inspect`查看