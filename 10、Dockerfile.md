# 10、Dockerfile


如果想把自己的项目打包成镜像，就要为这个项目编写`Dockerfile`文件

## 详细文档

[https://docs.docker.com/engine/reference/builder/](https://docs.docker.com/engine/reference/builder/)

[https://docs.docker.com/get-started/part2/](https://docs.docker.com/get-started/part2/)

## 编写 Dockerfile (**定制镜像**)

```dockerfile
# 基础镜像，基于什么语言/平台开发

FROM mcr.microsoft.com/dotnet/core/sdk:2.2

# 镜像信息

LABEL author="monk" email="monksoul@outlook.com" version="1.0"

# 创建一个工作目录（或者说是当前目录）

WORKDIR /app

# 拷贝所有文件到当前上下文目录

ADD . .

# 运行一条指令

RUN dotnet build "app.csproj" -c Release -o /app/push

# 切换到当前目录

WORKDIR /app/push

# 设置环境变量

ENV NAME=monk AUTHOR=monk

# 导出端口

EXPOSE 80

# 导出数据卷

VOLUME ["/uploads"]

# 构建完成后最后执行的命令

ENTRYPOINT [ "dotnet","app.dll" ]
```

----

## 一个简单Nginx的Dockerfile

```dockerfile
FROM nginx

ADD . /usr/share/nginx/html

EXPOSE 80
```

执行构建

```bash
docker build -t nginx-test:v1.0.0 .
```

运行

```bash
docker run --name nginx-testv1 -p 5002:80 -d nginx-test:v1.0.0
```

## 一个简单的ASP.NET Core 项目 Dockerfile

创建一个dotnet core 项目并发布到

```cmd
dotnet new razor

dotnet publish "asp.net core.csproj" -c Release -o ./publish
```

`Dockerfile` 代码如下

```dockerfile
FROM mcr.microsoft.com/dotnet/core/aspnet:2.2

WORKDIR /app

ADD ./publish /app

EXPOSE 80 443

ENTRYPOINT ["dotnet","asp.net core.dll"]
```

执行构建

```bash
docker build -t aspnetcore:v1.0.0 .
```

运行

```bash
docker run --name aspnetcorev1 -p 5003:80 -d aspnetcore:v1.0.0
```

## 多版本共存

如果我们修改了代码，我们只需要重新构建一个新版本即可，如

```bash
docker build -t nginx-test:v2.0.0 .

docker build -t aspnetcore:v2.0.0 .
```
