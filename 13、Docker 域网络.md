
@toc

# 13、Docker 域网络

默认情况下，我们运行的所有容器都是可以通讯的，因为这些容器的默认网络都是`host`模式（是本机模式），也就是可以相互访问，相互影响。

所以我们引用了 ==域网络==的概念，可以实现网络隔离

![wvk2v3ciht]($resource/wvk2v3ciht.png)

## Docker 内置网络

- `none`：顾名思义，没有任何网络，不能连接其他容器
- `host`：主机网络，也就是本机（宿主）的网络号段
- `bridge`：桥接网络，==默认网络`docker0`==
- `container`：Container 网络模式是 Docker 中一种较为特别的网络的模式。处于这个模式下的 Docker 容器会共享其他容器的网络环境，因此，至少这两个容器之间不存在网络隔离，而这两个容器又与宿主机以及除此之外其他的容器存在网络隔离。  

## 创建隔离网络，默认是`bridge`模式，集群需要创建`overlay`网络

```bash
docker network create mtr_network
```

## 应用隔离网络

```bash
docker run --network mtr_network
```

## 将正在运行的容器添加到网络中

```bash
docker network connect mtr_network my-nginx
```

## 域网络的作用

- 隔离网络号段，每一个项目网络相互独立
- 可以配置集群网络桥接